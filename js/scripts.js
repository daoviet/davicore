(function ($) {
    'use strict';

    var davicore = davicore || {};
    davicore.init = function () {
        davicore.$body = $(document.body),
            davicore.$window = $(window),
            davicore.$header = $('#site-header');
    };


    /**
     * Document ready
     */
    $(function () {
        davicore.init();
    });

})(jQuery);