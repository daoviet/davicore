module.exports = function (grunt) {
    // Project configuration
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        replace: {
            core: {
                src: [
                    '*.php',
                    'template-parts/*.php',
                    'template-parts/**/*.php',
                    'inc/*.php',
                    'inc/**/*.php',
                    'js/scripts.js',
                    'rtl.css',
                    'sass/style.scss',
                    'sass/woocommerce.scss'
                ],
                overwrite: true,
                replacements: [
                    {
                        from: 'davicore_',
                        to: '<%= pkg.prefix.func %>'
                    },
                    {
                        from: 'davicore-',
                        to: '<%= pkg.prefix.text %>' + '-'
                    },
                    {
                        from: 'DaviCore_',
                        to: '<%= pkg.prefix.class %>'
                    },
                    {
                        from: 'DaviCore - ',
                        to: '<%= pkg.prefix.widget %>'
                    },
                    {
                        from: 'davicore',
                        to: "<%= pkg.prefix.text %>"
                    },
                    {
                        from: 'THEME_DOMAIN',
                        to: "<%= pkg.name %>"
                    },
                    {
                        from: 'THEME_VERSION',
                        to: "<%= pkg.date %>"
                    },
                    {
                        from: 'THEME_NAME',
                        to: "<%= pkg.theme.name %>"
                    },
                    {
                        from: 'THEME_URI',
                        to: "<%= pkg.theme.uri %>"
                    },
                    {
                        from: 'THEME_AUTHOR_URI',
                        to: "<%= pkg.theme.author_uri %>"
                    },
                    {
                        from: 'THEME_AUTHOR',
                        to: "<%= pkg.theme.author %>"
                    },
                    {
                        from: 'THEME_DESC',
                        to: "<%= pkg.theme.desc %>"
                    },
                    {
                        from: '@package DaviCore',
                        to: "@package <%= pkg.pkgname %>"
                    }
                ]

            },
        },

        autoprefixer: {
            options: {
                browsers: ['Android >= 2.1', 'Chrome >= 21', 'Explorer >= 8', 'Firefox >= 17', 'Opera >= 12.1', 'Safari >= 6.0']
            },
            core: {
                expand: true,
                src: ['style.css', 'rtl.css']
            }
        },

        sass: {
            dist: {
                sourcemap: 'none',
                files: {
                    "style.css": "sass/style.scss"
                },
                lineNumbers: 4
            }
        },

        uglify: {
            core: {
                expand: true,
                cwd: 'js/',
                dest: 'js/',
                ext: '.min.js',
                src: [
                    '*.js',
                    '*.*.js',
                    '!*.min.js',
                ]
            }
        },


        watch: {
            jsdev: {
                options: {livereload: true},
                files: ['js/scripts.js'],
                tasks: ['uglify']
            },
            css: {
                files: ['sass/*.scss', 'sass/**/*.scss'],
                tasks: ['sass', 'autoprefixer']
            },
            core: {
                options: {livereload: true},
                files: ['*.php', '*/*.php', 'parts/*.php', 'inc/*.php', 'inc/*/*.php', '!inc/backend/*.php'],
                tasks: []
            },
            livereload: {
                options: {livereload: true},
                files: ['style.css', 'rtl.css']
            }

        }
    });

    // Load tasks.
    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

    // Register makepot task
    grunt.registerTask('buildstart', ['replace:core']);

    // Register default tasks
    grunt.registerTask('default', ['sass', 'uglify', 'watch']);
};