<?php
/**
 * Custom layout functions
 *
 * @package DaviCore
 */


/**
 * Get classes for content area
 *
 * @since  1.0
 *
 * @return string of classes
 */

if ( ! function_exists( 'davicore_content_container_class' ) ) :
	function davicore_content_container_class() {
		return 'container';
	}

endif;


if ( ! function_exists( 'davicore_get_layout' ) ) :
	function davicore_get_layout() {
		$layout = 'full-content';
		$layout = 'sidebar-content';

		return apply_filters( 'davicore_site_layout', $layout );
	}

endif;

/**
 * Get Bootstrap column classes for content area
 *
 * @since  1.0
 *
 * @return array Array of classes
 */

if ( ! function_exists( 'davicore_get_content_columns' ) ) :
	function davicore_get_content_columns( $layout = null ) {
		$layout  = $layout ? $layout : davicore_get_layout();
		$classes = array( 'col-md-9', 'col-sm-12', 'col-xs-12' );

		if ( $layout == 'full-content' ) {
			$classes = array( 'col-md-12' );
		}

		return $classes;
	}

endif;

/**
 * Echos Bootstrap column classes for content area
 *
 * @since 1.0
 */

if ( ! function_exists( 'davicore_content_columns' ) ) :
	function davicore_content_columns( $layout = null ) {
		echo implode( ' ', davicore_get_content_columns( $layout ) );
	}
endif;
