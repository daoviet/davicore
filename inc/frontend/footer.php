<?php
/**
 * Hooks for template footer
 *
 * @package DaviCore
 */


/**
 * Show footer
 */

if ( ! function_exists( 'davicore_show_footer' ) ) :
	function davicore_show_footer() {
		$footer_layout = 1;
		get_template_part( 'template-parts/footers/layout', $footer_layout );
	}

endif;

add_action( 'davicore_footer', 'davicore_show_footer' );