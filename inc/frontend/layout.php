<?php
/**
 * Custom layout functions  by hooking templates
 *
 * @package DaviCore
 */


/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 *
 * @return array
 */
function davicore_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}


	return $classes;
}

add_filter( 'body_class', 'davicore_body_classes' );

/**
 * Print the open tags of site content container
 */

if ( ! function_exists( 'davicore_open_site_content_container' ) ) :
	function davicore_open_site_content_container() {

		printf( '<div class="%s"><div class="row">', esc_attr( apply_filters( 'davicore_site_content_container_class', davicore_content_container_class() ) ) );
	}
endif;

add_action( 'davicore_after_site_content_open', 'davicore_open_site_content_container' );

/**
 * Print the close tags of site content container
 */

if ( ! function_exists( 'davicore_close_site_content_container' ) ) :
	function davicore_close_site_content_container() {
		print( '</div></div>' );
	}

endif;

add_action( 'davicore_before_site_content_close', 'davicore_close_site_content_container' );

