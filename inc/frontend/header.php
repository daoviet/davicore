<?php
/**
 * Hooks for template header
 *
 * @package DaviCore
 */


/**
 * Enqueue scripts and styles.
 */
function davicore_scripts() {
	/**
	 * Register and enqueue styles
	 */
	wp_register_style( 'davicore-fonts', davicore_fonts_url(), array(), 'THEME_VERSION' );
	wp_register_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '3.3.7' );
	wp_register_style( 'font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css', array(), '4.7.0' );
	wp_enqueue_style( 'THEME_DOMAIN', get_template_directory_uri() . '/style.css', array(
		'davicore-fonts',
		'font-awesome',
		'bootstrap',
	), 'THEME_VERSION' );

	/**
	 * Register and enqueue scripts
	 */

	wp_enqueue_script( 'html5shiv', get_template_directory_uri() . '/js/plugins/html5shiv.min.js', array(), '3.7.2' );
	wp_script_add_data( 'html5shiv', 'conditional', 'lt IE 9' );

	wp_enqueue_script( 'respond', get_template_directory_uri() . '/js/plugins/respond.min.js', array(), '1.4.2' );
	wp_script_add_data( 'respond', 'conditional', 'lt IE 9' );

	wp_enqueue_script( 'THEME_DOMAIN', get_template_directory_uri() . "/js/scripts.js", array(
		'jquery',
	), 'THEME_VERSION', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}

add_action( 'wp_enqueue_scripts', 'davicore_scripts' );

/**
 * Display the site header
 *
 * @since 1.0.0
 */
if ( ! function_exists( 'davicore_show_header' ) ) :
	function davicore_show_header() {
		$header_layout = 1;
		get_template_part( 'template-parts/headers/layout', $header_layout );
	}
endif;
add_action( 'davicore_header', 'davicore_show_header' );
